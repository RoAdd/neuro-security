from os import getcwd
from os.path import isfile, join

import config

import numpy as np
import theano
from keras.callbacks import EarlyStopping
from keras.layers.convolutional import (Convolution2D, MaxPooling2D)
from keras.layers.core import (Dense, Dropout, Flatten, Reshape)
from keras.models import Sequential

import dataset

def build_model():
    model = Sequential()
    model.add()

    return model


def get_activations(model, layer, X_batch):
    get_activations = theano.function([model.layers[0].input], model.layers[
        layer].get_output_shape_for(None), allow_input_downcast=True)
    activations = get_activations(X_batch)  # same result as above
    return activations

if __name__ == '__main__':
    data = dataset.read_data_sets(size=[1000, 1000])
    images, labels = data.get_set()

    print data.get_class_weights()

    images_test, labels_test = dataset.read_data_sets(
        one_hot=False, test=True).get_set()

    images = images.reshape([-1, 1, 96, 48])
    images_test = images_test.reshape([-1, 1, 96, 48])

    encoder_layers, decoder_layers, classification_layers = build_model()
    if not isfile(config.NEW_MODEL_SAVE_PATH):
        images_tmp = np.copy(images)
        for encoder_layer, decoder_layer in zip(encoder_layers, decoder_layers):
            # Create AE and training
            ae = Sequential()
            ae.add(encoder_layer)
            ae.add(decoder_layer)
            ae.compile(loss='mean_squared_error', optimizer='sgd')
            ae.summary()
            ae.fit(images_tmp, images_tmp, batch_size=1, nb_epoch=20, verbose=1)
            # Store trainined weight and update training data
            images_tmp = get_activations(ae, 0, images_tmp)
        del images_tmp
    del images
    del labels
    del data
    data = dataset.read_data_sets(size=[6000, None])
    images, labels = data.get_set()
    images = images.reshape([-1, 1, 96, 48])

    model = Sequential()
    for encoder in encoder_layers:
        weights = encoder.get_weights()
        model.add(encoder)
        encoder.set_weights(weights)
    for layer in classification_layers:
        model.add(layer)

    if isfile(config.NEW_MODEL_SAVE_PATH):
        model.load_weights(config.NEW_MODEL_SAVE_PATH)
    else:
        model.save_weights(config.NEW_MODEL_SAVE_PATH, overwrite=True)

    model.compile(loss='binary_crossentropy',
                  optimizer='sgd', class_mode='binary')
    model.summary()
    score = model.evaluate(images_test, labels_test,
                           show_accuracy=True, verbose=1)
    print('Test score before fine turning:', score[0])
    print('Test accuracy before fine turning:', score[1])
    model.fit(images, labels, batch_size=1, nb_epoch=50, class_weight=data.get_class_weights(),
              show_accuracy=True, validation_data=(images_test, labels_test), verbose=1, callbacks=[EarlyStopping(patience=3)])
    score = model.evaluate(images_test, labels_test,
                           show_accuracy=True, verbose=1)
    print('Test score after fine turning:', score[0])
    print('Test accuracy after fine turning:', score[1])
    model.save_weights(config.NEW_MODEL_SAVE_PATH, overwrite=True)
