from os import getcwd, listdir, unlink, makedirs
from os.path import join, isfile, dirname, exists

from time import strftime, gmtime
import config
import numpy as np
from keras import models
from keras.callbacks import EarlyStopping
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import containers
from keras.layers.convolutional import (Convolution2D, MaxPooling2D,
                                        UpSampling2D)
from keras.layers.core import Activation
from keras.layers.core import AutoEncoder
from keras.layers.core import Dense
from keras.layers.core import Dropout
from keras.layers.core import Flatten
from keras.layers.core import Reshape

import dataset
from helper.decode_elements import DePool2D
from helper.decode_elements import Deconvolution2D
from helper.decode_elements import DependentDense
from helper.helper import show_representations

from PIL import Image

from cae_1 import build_model


def ensure_dir(f):
    d = dirname(f)
    if not exists(d):
        makedirs(d)


def clear_folder(f):
    for the_file in listdir(f):
        file_path = join(f, the_file)
        try:
            if isfile(file_path):
                unlink(file_path)
        except Exception, e:
            print e

if __name__ == '__main__':
    encoder_layers, decoder_layers, classification_layers = build_model()
    model = Sequential()
    for encoder in encoder_layers:
        model.add(encoder)
    for layer in classification_layers:
        model.add(layer)

    model.load_weights(SAVE_PATH)
    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop', class_mode='binary')

    images_test, labels_test = dataset.read_data_sets(  # path=join(getcwd(), "..", "eval_data"), only_positives=False,
        one_hot=False, test=True).get_set()

    images_test = images_test.reshape([-1, 1, 96, 48])
    values = model.predict(images_test).flatten()
    print values
    prediction = np.array([1 if i > THRESHOLD else 0 for i in values])
    if SAVE_FALSE_RESPONSE:
        ensure_dir(FALSE_RESOPONSE_FOLDER)
        ensure_dir(join(FALSE_RESOPONSE_FOLDER, "pos"))
        ensure_dir(join(FALSE_RESOPONSE_FOLDER, "neg"))
        clear_folder(join(FALSE_RESOPONSE_FOLDER, "pos"))
        clear_folder(join(FALSE_RESOPONSE_FOLDER, "neg"))

        indices = [i for i, v in enumerate(prediction) if v != labels_test[i]]
        for i in indices:
            Image.fromarray(np.multiply(
                images_test[i], 255).reshape(96,
                                             48)).convert('RGB').save(join(FALSE_RESOPONSE_FOLDER, "pos" if
                                                                           labels_test[i] else "neg", strftime("%a, %d %b %Y %H:%M:%S", gmtime()) +
                                                                           "-" + str(i) + ".png"))
    print("Accuracy: " + str(np.sum(prediction ==
                                    labels_test) * 100.0 / len(labels_test)))
