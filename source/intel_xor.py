import numpy as np

a = np.array([0b01100000, 0b01001000, 0b11100100, 0b11000100, 0b00000000, 0b01000100, 0b01011100, 0b11001100, 0b01111100, 0b01110000, 0b01101000, 0b01001000], dtype=np.uint8)

print "decimal: "
print a

print "sum:"
print a.sum()

print "and:"
b = 0b00000000
for i in a:
    b = b | i
print np.binary_repr(b, width=8)
