from os import getcwd, listdir
from os.path import isfile, join

import config

import numpy as np
import theano
from keras import models
from keras.callbacks import EarlyStopping
from keras.datasets import mnist
from keras.layers import containers
from keras.layers.convolutional import (Convolution2D, AveragePooling2D,
                                        UpSampling2D, ZeroPadding2D)
from keras.layers.core import (Activation, AutoEncoder, Dense, Dropout,
                               Flatten, Reshape)
from keras.models import Sequential

from keras import backend as K
import dataset


def loss(y_true, y_pred):
    return K.sqrt(K.sum(K.square(y_pred - y_true), axis=-1))


def regulariser(x, beta):
    shape = x.shape
    s = 0
    for c in range(shape[0]):
        for i in range(shape[1]):
            for j in range(shape[2]):
                ni = (i + 1) % shape[1]
                nj = (j + 1) % shape[2]
                s += pow(pow(x[c, i, nj] - x[c, i, j], 2) +
                         pow(x[c, ni, j] - x[c, i, j], 2), beta)
    return s


def objective(y_true, x):


def build_model():
    model = Graph()
    model.add_input(name='input', input_shape=(3, 224, 224))
    # Conv_1
    model.add_node(Convolution2D(64, 3, 3, border_mode='same',
                   activation="relu"), name="conv_1_1", input="input")
    model.add_node(Convolution2D(64, 3, 3, border_mode='same',
                   activation="relu"), name="conv_1_2", input="conv_1_1")
    model.add_node(AveragePooling2D(pool_size=(2, 2), name="pool_1", input="conv_1_2")
    # Conv_2
    model.add_node(Convolution2D(128, 3, 3, border_mode='same',
                   activation="relu"), name="conv_2_1", input="pool_1")
    model.add_node(Convolution2D(128, 3, 3, border_mode='same',
                   activation="relu"), name="conv_2_2", input="conv_2_1")
    model.add_node(AveragePooling2D(pool_size=(2, 2), name="pool_2", input="conv_2_2")
    # Conv_3
    model.add_node(Convolution2D(256, 3, 3, border_mode='same',
                   activation="relu"), name="conv_3_1", input="pool_2")
    model.add_node(Convolution2D(256, 3, 3, border_mode='same',
                   activation="relu"), name="conv_3_2", input="conv_3_1")
    model.add_node(Convolution2D(256, 3, 3, border_mode='same',
                   activation="relu"), name="conv_3_3", input="conv_3_2")
    model.add_node(Convolution2D(256, 3, 3, border_mode='same',
                   activation="relu"), name="conv_3_4", input="conv_3_3")
    model.add_node(AveragePooling2D(pool_size=(2, 2), name="pool_3", input="conv_3_4")
    # Conv_4
    model.add_node(Convolution2D(512, 3, 3, border_mode='same',
                   activation="relu"), name="conv_4_1", input="pool_3")
    model.add_node(Convolution2D(512, 3, 3, border_mode='same',
                   activation="relu"), name="conv_4_2", input="conv_4_1")
    model.add_node(Convolution2D(512, 3, 3, border_mode='same',
                   activation="relu"), name="conv_4_3", input="conv_4_2")
    model.add_node(Convolution2D(512, 3, 3, border_mode='same',
                   activation="relu"), name="conv_4_4", input="conv_4_3")
    model.add_node(AveragePooling2D(pool_size=(2, 2), name="pool_4", input="conv_4_4")
    # Conv_5
    model.add_node(Convolution2D(512, 3, 3, border_mode='same',
                   activation="relu"), name="conv_5_1", input="pool_4")
    model.add_node(Convolution2D(512, 3, 3, border_mode='same',
                   activation="relu"), name="conv_5_2", input="conv_5_1")
    model.add_node(Convolution2D(512, 3, 3, border_mode='same',
                   activation="relu"), name="conv_5_3", input="conv_5_2")
    model.add_node(Convolution2D(512, 3, 3, border_mode='same',
                   activation="relu"), name="conv_5_4", input="conv_5_3")
    model.add_node(AveragePooling2D(pool_size=(2, 2), name="pool_5", input="conv_5_4")
