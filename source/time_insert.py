# -- coding: utf-8 --
import config
from datetime import datetime
import json
import urllib
import urllib2
import webbrowser

def getTime(zone, persons):
    month_arr = ["january","february","march","april","may","june","july","august","september","october","november","december"]

    my_time_arr =  datetime.now().strftime('%Y-%m-%d-%H-%M').split("-",5)
    my_db = my_time_arr[0]+"_"+month_arr[int(my_time_arr[1])-1]
    my_arr = [my_time_arr[3], my_time_arr[4]]
    time = my_time_arr[0]+"-"+my_time_arr[1]+"-"+my_time_arr[2]

    query_args = { 'db':my_db, 'data':my_arr ,'zone' : zone,'time' : time, 'persons': persons}


    encoded_args = urllib.urlencode(query_args)
    url = "http://"+config.IP+"/index.php/pythonController"
    print urllib2.urlopen(url, encoded_args).read()


  ##  webbrowser.open_new_tab('http://localhost/index.php/pythonController')


# getTime("hol", 5)
