import config

from collections import Counter
from os import getcwd, listdir
from os.path import join
from random import uniform as randfloat
from random import randint
import config
import numpy as np
from PIL import Image, ImageEnhance, ImageOps

WIDTH = None
HEIGHT = None
NUM_IMAGES = 1
LOADED = None
DATA = None
CHANNELS = None
INIT_WIDTH = None
INIT_HEIGHT = None

def init_consts():
    global WIDTH
    global HEIGHT
    global CHANNELS
    WIDTH = 48
    HEIGHT = 96
    CHANNELS = 1


def read_img(path, mode, padding=(0, 0, 0, 0), batch=False, distort=False, input_size=[None, None], whitening=False):
    img = Image.open(path, "r")
    img = img.convert("L")

    if mode == "pos":
        img = img.crop((padding[0], padding[1], img.size[0] - padding[2],
                        img.size[1] - padding[3])).resize((WIDTH, HEIGHT), Image.BILINEAR)
    elif mode == "neg":
        r1 = randint(padding[0], img.size[0] - padding[2] - WIDTH)
        r2 = randint(padding[1], img.size[1] - padding[3] - HEIGHT)
        img = img.crop((r1, r2, r1 + WIDTH, r2 + HEIGHT)
                       ).resize((WIDTH, HEIGHT), Image.BILINEAR)

    if distort:
        contrast = ImageEnhance.Contrast(img)
        contrast.enhance(randfloat(0.2, 1.8))

        brightness = ImageEnhance.Brightness(img)
        brightness.enhance(randfloat(0.5, 1.5))

        if randint(0, 1) == 1:
            ImageOps.mirror(img)

    data = np.array(img.getdata())
    data = data.astype(np.float32)
    data = np.multiply(data, 1.0 / 255.0)
    # data = np.rollaxis(data, 2)
    if whitening:
        data = tf.image.per_image_whitening(data)
    img.close()

    if not batch:
        log(path)

    return data


def read_data(filename, distort=False, test=False, size=[None, None], input_size=[None, None], one_hot=False, whitening=False, only_positives=False):
    global NUM_IMAGES
    global LOADED
    LOADED = 0

    files_pos = listdir(join(filename, "pos"))
    files_neg = listdir(join(filename, "neg"))
    numbers = np.copy(size)
    if size[0] is None:
        numbers[0] = len(files_pos)

    if size[1] is None:
        numbers[1] = len(files_neg)

    NUM_IMAGES = numbers[0] + numbers[1]

    positives = []
    negatives = []

    for i in range(0, numbers[0]):
        try:
            positives.append(read_img(join(join(filename, "pos"), files_pos[
                            i]), "pos", distort=distort, input_size=input_size, whitening=whitening))
        except:
            log("Failed")           

    if not only_positives:
        for i in range(0, numbers[1]):
            try:
                negatives.append(read_img(join(join(filename, "neg"), files_neg[
                                i]), "neg", distort=distort, input_size=input_size, whitening=whitening))
            except:
                log("Failed")        

    labels = np.array([[0, 1] if one_hot else 1 for f in positives] +
                      [[1, 0] if one_hot else 0 for f in negatives]).astype(np.int64)

    data = np.array(positives + negatives)

    return data, labels


def read_data_batch(filename, test=False, size=[None, None], one_hot=False, only_positives=False):
    global NUM_IMAGES
    global LOADED
    LOADED = 0

    files_pos = listdir(join(filename, "pos"))
    files_neg = listdir(join(filename, "neg"))

    numbers = np.copy(size)
    if size[0] is None:
        numbers[0] = len(files_pos)

    if size[1] is None:
        numbers[1] = len(files_neg)

    NUM_IMAGES = numbers[0] + numbers[1]

    positives = []
    negatives = []

    for i in range(0, numbers[0]):
        positives.append(join(join(filename, "pos"), files_pos[i]))
        log(files_pos[i])

    if not only_positives:
        for i in range(0, numbers[1]):
            negatives.append(join(join(filename, "neg"), files_neg[i]))
            log(files_neg[i])

    labels = np.array([[0, 1] if one_hot else 1 for f in positives] +
                      [[1, 0] if one_hot else 0 for f in negatives]).astype(np.int64)
    files = np.array(positives + negatives)

    return files, labels


def log(path):
    global LOADED
    if LOADED is None:
        LOADED = 0
    LOADED += 1.0
    print LOADED / NUM_IMAGES * 100, "% -", path


class DataSet(object):

    def __init__(self, images, labels):
        self._images = images
        self._num_examples = len(labels)
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch = 0

        perm = np.arange(self._num_examples)
        np.random.shuffle(perm)
        self._images = self._images[perm]
        self._labels = self._labels[perm]

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def get_set(self):
        return self._images, self._labels

    def get_class_weights(self):
        cnt = Counter()
        for i in self._labels:
            cnt[i] += 1
        return {0: cnt[0] * 1.0 / len(self._labels), 1: cnt[1] * 1.0 / len(self._labels)}

    def next_batch(self, batch_size):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            perm = np.arange(self._num_examples)
            np.random.shuffle(perm)
            self._images = self._images[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        return self._images[start:end], self._labels[start:end]

    def generator_batch(self):
        for image, label in zip(self._images, self._labels):
            yield image, label


class DataSetBatched:

    def __init__(self, paths, labels, distort=False, input_size=[None, None], whitening=False):
        self._paths = paths
        self._distorted = distort
        self._input_size = input_size
        self._labels = labels
        self._num_examples = len(labels)
        self._epochs_completed = 0
        self._index_in_epoch = 0
        self._whitening = whitening
        perm = np.arange(self._num_examples)
        np.random.shuffle(perm)
        self._paths = self._paths[perm]
        self._labels = self._labels[perm]

    @property
    def paths(self):
        return self._paths

    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def get_class_weights(self):
        cnt = Counter()
        for i in self._labels:
            cnt[i] += 1
        return [len(self._labels) * 1.0 / cnt[0], len(self._labels) * 1.0 / cnt[1]]

    def get_set(self):
        return np.array([read_img(f, "neg", batch=True, distort=self._distorted, input_size=self._input_size) for f in self._paths]), \
            self._labels

    def next_batch(self, batch_size):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            perm = np.arange(self._num_examples)
            np.random.shuffle(perm)
            self._paths = self._paths[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        return np.array([read_img(f, "neg", batch=True, distort=self._distorted, input_size=self._input_size, whitening=self._whitening) for f in self._paths[start:end]]).reshape(batch_size, HEIGHT, WIDTH, CHANNELS).astype(np.float32), \
            self._labels[start:end]

    def generator_batch(self):
        for image, label in zip(self._images, self._labels):
            yield read_img(image, "neg", batch=True, distort=self._distorted, input_size=self._input_size, whitening=self._whitening), label


def read_data_sets(test=False, path=None, batch=False, distort=False, size=[None, None], input_size=[None, None], one_hot=False, only_positives=False, whitening=False):
    init_consts()
    if(not test):
        if(path is None):
            path = config.DATASET_PATH
        if batch:
            files, labels = read_data_batch(
                path, size=size, one_hot=one_hot, only_positives=only_positives)
            data = DataSetBatched(
                files, labels, distort=distort, input_size=input_size, whitening=whitening)
        else:
            img, labels = read_data(path, distort=distort, size=size,
                                    input_size=input_size, one_hot=one_hot, whitening=whitening, only_positives=only_positives)
            data = DataSet(img, labels)
    else:
        if(path is None):
            path = config.DATASET_TEST_PATH
        img, labels = read_data(path, distort=distort, test=True, size=size,
                                input_size=input_size, one_hot=one_hot, whitening=whitening, only_positives=only_positives)
        data = DataSet(img, labels)
    return data
