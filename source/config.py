from os.path import join, isfile, dirname
from os import getcwd, environ
import sys
import numpy as np
import json
import os
# Library paths

def add_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)

this_dir = dirname(__file__)

# Add caffe to PYTHONPATH
caffe_path = "/home/roadd/Compile/caffe/python"
add_path(caffe_path)

# Add neuro-security to PYTHONPATH
neuro_path = "/home/roadd/Workspace/python-workspace/camera-detection-coolest-projects"
add_path(neuro_path)

# Add yolo to PYTHONPATH
# yolo_path = join(getcwd(), 'py-faster-rcnn', 'lib')
# add_path(lib_yolo)

# # Add caffe2 to PYTHONPATH
# caffe2_path = "/home/roadd/Compile/caffe2/gen"
# # add_path(caffe2_path)

# Add BING-Objectness to PYTHONPATH
bing_path = join(getcwd(), 'BING-Objectness')
add_path(bing_path)

# Environment Variables

environ["THEANO_FLAGS"] = "mode=FAST_RUN, \
                           device=gpu, \
                           floatX=float32,\
                           optimizer=fast_compile"
environ["CUDA_ROOT"] = "/usr/local/cuda"

# Constants
IP = "192.168.0.2"
MODEL_SAVE_PATH = "/home/roadd/Data/Models/pedestrian-classification.model"
NEW_MODEL_SAVE_PATH = "/home/roadd/Data/Models/new-pedestrian-classification.model"
LOG_PATH = "/home/roadd/Logs/"
DATASET_PATH = "/home/roadd/Data/Datasets/daimler/train/"
DATASET_TEST_PATH = "/home/roadd/Data/Datasets/daimler/test/"
CLASSIFICATION_THRESHOLD = 0.3

YOLO_MODEL_PATH = join(getcwd(), "caffe-yolo", "prototxt", 'yolo_small_deploy.prototxt')
YOLO_WEIGHTS_PATH = join(getcwd(), "caffe-yolo", "weights", 'yolo_small.caffemodel')

BING_1ST_WEIGHTS_PATH = join(getcwd(), 'BING-Objectness', 'doc', "weights.txt")
BING_1ST_WEIGHTS_PATH = join(getcwd(), 'BING-Objectness', 'doc', "weights_mine.txt")
BING_SIZES_INDECES_FN_PATH = join(getcwd(), 'BING-Objectness', 'doc', "sizes.txt")
BING_SIZES_INDECES_FN_PATH = join(getcwd(), 'BING-Objectness', 'doc', "sizes_mine.txt")
BING_2ND_WEIGHTS_PATH = join(getcwd(), 'BING-Objectness', 'doc', "2nd_stage_weights.json")
BING_2ND_WEIGHTS_PATH = join(getcwd(), 'BING-Objectness', 'doc', "2nd_stage_weights_mine.json")
BING_1ST_BBS_COUNT = 100
BING_2ND_BBS_COUNT = 300

BING_1ST_WEIGHTS = None
BING_2ND_WEIGHTS = None
BING_SIZES_INDECES_FN = None

def bing_init():
    global BING_1ST_WEIGHTS
    global BING_2ND_WEIGHTS
    global BING_SIZES_INDECES_FN

    if not os.path.exists(BING_1ST_WEIGHTS_PATH):
        print "The weights for the first stage does not exist!"
        sys.exit(2)

    if not os.path.exists(BING_SIZES_INDECES_FN_PATH):
        print "The sizes indices file does not exist!"
        sys.exit(2)

    if not os.path.exists(BING_2ND_WEIGHTS_PATH):
        print "The weights for the second stage does not exist!"
        sys.exit(2)

    BING_1ST_WEIGHTS = np.genfromtxt(BING_1ST_WEIGHTS_PATH, delimiter=",").astype(np.float32)

    f = open(BING_2ND_WEIGHTS_PATH)
    w_str = f.read()
    f.close()
    BING_2ND_WEIGHTS = json.loads(w_str)

    BING_SIZES_INDECES_FN = np.genfromtxt(BING_SIZES_INDECES_FN_PATH, delimiter=",").astype(np.float32)
