import math
import numpy as np
import cv2
import matplotlib.pyplot as plt
from scipy.ndimage.filters import sobel

FgC = 1
S = 2
H = 3
BgC = 4

FgE = 5
BgE = 6

LF = 7
DF = 8
BB = 9

BgI = 10
FgI = 11

class BCM(object):
    def __init__(self, frames, n_frames):
        frames = frames.astype(np.float32)
        self.height, self.width, self.channels = frames[0].shape
        self.stddev = self.get_pixel_stddev(frames)
        self.expected = self.get_pixel_mean(frames)

        alphas = None
        cds = None
        for i in range(n_frames):
            print i
            if alphas is None:
                alphas = self.brightness_dist(frames[i]).reshape(1, self.height, self.width, 1)
                cds = self.color_dist(frames[i]).reshape(1, self.height, self.width, 1)
            else:
                alphas = np.append(alphas, self.brightness_dist(frames[i]).reshape(1, self.height, self.width, 1), axis=0)
                cds = np.append(cds, self.color_dist(frames[i]).reshape(1, self.height, self.width, 1), axis=0)

        self.a = self.calculate_a(alphas).reshape(self.height, self.width)
        self.b = self.calculate_b(cds).reshape(self.height, self.width)

        norm_alphas = None
        norm_cds = None

        for i in range(n_frames):
            print i
            if norm_alphas is None:
                norm_alphas = self.norm_brightness_dist(frames[i]).reshape(1, self.height, self.width, 1)
                norm_cds =  self.norm_color_dist(frames[i]).reshape(1, self.height, self.width, 1)
            else:
                norm_alphas = np.append(norm_alphas, self.norm_brightness_dist(frames[i]).reshape(1, self.height, self.width, 1), axis=0)
                norm_cds = np.append(norm_cds, self.norm_color_dist(frames[i]).reshape(1, self.height, self.width, 1), axis=0)

        self.t_a1 = np.sort(norm_alphas.flatten())[int(0.99 * norm_alphas.size)]
        self.t_a2 = np.sort(norm_alphas.flatten())[int(0.01 * norm_alphas.size)]
        self.t_l = 4 * self.t_a1
        self.t_d = 4 * self.t_a2
        self.t_cd = 1 * np.max(norm_cds)

    def get_pixel_stddev(self, set):
        return np.sqrt(np.sum(np.power(set * 1. - set.mean(axis=0), 2), axis=0) / set.shape[0])

    def get_pixel_mean(self, set):
        return set.mean(axis=0)

    def brightness_dist(self, given):
        return np.sum(self.expected * 1. * given / np.power(self.stddev, 2),axis=2) / np.sum(np.power(self.expected / self.stddev, 2),axis=2)

    def norm_brightness_dist(self, given):
        alpha = self.brightness_dist(given)
        return alpha / self.a

    def color_dist(self, given):
        alpha = self.brightness_dist(given)
        return np.arcsin(np.sqrt(np.sum(np.power((given * 1. - alpha.reshape(self.height, self.width, 1) * self.expected) / self.stddev, 2), axis=2)) / np.sqrt(np.sum(np.power(given / self.stddev, 2), axis=2)))

    def norm_color_dist(self, given):
        cd = self.color_dist(given)
        return cd * 1.0 / self.b

    def calculate_a(self, alphas):
        return np.sqrt(np.sum(np.power(alphas - 1, 2), axis=0) / alphas.shape[0])

    def calculate_b(self, cds):
        return np.sqrt(np.sum(np.power(cds, 2), axis=0) / cds.shape[0])

    def decide_normal(self, given, given_mask=None):
        norm_a = self.norm_brightness_dist(given).reshape(self.height, self.width, 1)
        norm_cd = self.norm_color_dist(given).reshape(self.height, self.width, 1)
        if given_mask is not None:
            mask = np.where((np.min(given_mask, axis=2) == -1).reshape(self.height, self.width, 1), BgC,
                   np.where((norm_cd > self.t_cd), FgC,
                   np.where((norm_a < self.t_a1), S,
                   np.where((norm_a > self.t_a2), H,
                   BgC))))
        else:
            mask = np.where((norm_cd > self.t_cd), FgC,
                   np.where((norm_a < self.t_a1), S,
                   np.where((norm_a > self.t_a2), H,
                   BgC)))
        return np.array(mask, dtype=np.uint8)

    def decide_extrem(self, given, given_mask=None):
        norm_a = self.norm_brightness_dist(given).reshape(self.height, self.width, 1)
        if given_mask is not None:
            mask = np.where((np.min(given_mask, axis=2) == -1).reshape(self.height, self.width, 1), BB,
                   np.where((norm_a < self.t_d), DF,
                   np.where((norm_a > self.t_l), LF,
                   BB)))
        else:
            mask = np.where((norm_a < self.t_d), DF,
                   np.where((norm_a > self.t_l), LF,
                   BB))
        return np.array(mask)


class BEM(object):
    def __init__(self, frames, n_frames):
        frames = frames.astype(np.float32)
        self.height, self.width, self.channels = frames[0].shape

        gxs = None
        gys = None
        for i in range(n_frames):
            print i
            if gxs is None:
                gxs = sobel(frames[i], axis=0, mode="constant", cval=0).reshape(1, self.height, self.width, self.channels)
                gys = sobel(frames[i], axis=1, mode="constant", cval=0).reshape(1, self.height, self.width, self.channels)
            else:
                gxs = np.append(gxs, sobel(frames[i], axis=0, mode="constant", cval=0).reshape(1, self.height, self.width, self.channels), axis=0)
                gys = np.append(gys, sobel(frames[i], axis=1, mode="constant", cval=0).reshape(1, self.height, self.width, self.channels), axis=0)

        mean_gx = self.get_pixel_mean(gxs)
        mean_gy = self.get_pixel_mean(gys)

        stddev_gx = self.get_pixel_stddev(gxs)
        stddev_gy = self.get_pixel_stddev(gys)
        self.mag_mean = self.get_mag(mean_gx, mean_gy)
        self.mag_stddev = self.get_mag(stddev_gx, stddev_gy)

        self.orient_mean = self.get_orientation(mean_gx, mean_gy)
        self.orient_stddev = self.get_orientation(stddev_gx, stddev_gy)

        e = 1e-4
        avg_orient_stddev = np.mean(self.orient_stddev)
        avg_mag_stddev = np.mean(self.mag_stddev)
        self.t_e = np.where(3 * self.mag_stddev > e, 3 * self.mag_stddev, e)
        self.t_o = np.where(1.5 * self.orient_stddev > avg_orient_stddev, 1.5 * self.orient_stddev, avg_orient_stddev)
        self.t_g = np.where(5 * self.mag_stddev > avg_mag_stddev, 5 * self.mag_stddev, avg_mag_stddev)

    def get_mag_orient(self, given):
        given = given.astype(np.float32)
        gx = sobel(given, axis=0, mode="constant", cval=0)
        gy = sobel(given, axis=1, mode="constant", cval=0)
        return self.get_mag(gx, gy), self.get_orientation(gx, gy)

    def get_pixel_stddev(self, set):
        return np.sqrt(np.sum(np.power(set - set.mean(axis=0), 2), axis=0) / set.shape[0])

    def get_pixel_mean(self, set):
        return set.mean(axis=0)

    def get_mag(self, gx, gy):
        return np.sqrt(np.power(gx, 2) + np.power(gy, 2))

    def get_orientation(self, gx, gy):
        return np.arctan2(gx, gy)

    def calculate_fo_fg(self, given):
        mag, orient = self.get_mag_orient(given)
        fo = np.logical_or.reduce(((self.t_e < mag) & (self.t_e < self.mag_mean)) & (self.t_o < np.abs(orient - self.orient_mean)), axis = 2)
        fg = np.logical_or.reduce((~((self.t_e < mag) & (self.t_e < self.mag_mean))) & (self.t_g < np.abs(mag - self.mag_mean)), axis = 2)
        return fo, fg

    def decide(self, given, given_mask=None):
        fo, fg = self.calculate_fo_fg(given)
        if given_mask is not None:
            mask = np.where((np.min(given_mask, axis=2) == -1), BgE,
                   np.where(fg, FgE, BgE))
        else:
            mask = np.where(fo | fg, FgE, BgE)
        return np.array(mask)

class BIM(object):
    def __init__(self, frames, n_frames):
        frames = frames.astype(np.float32)

        self.height, self.width, self.channels = frames[0].shape

        grays = None
        for i in range(n_frames):
            print i
            if grays is None:
                grays = cv2.cvtColor(frames[i], cv2.COLOR_BGR2GRAY).reshape(1, self.height, self.width, 1)
            else:
                grays = np.append(grays, cv2.cvtColor(frames[i], cv2.COLOR_BGR2GRAY).reshape(1, self.height, self.width, 1), axis=0)

        self.mean = self.get_pixel_mean(grays)
        self.stddev = self.get_pixel_stddev(grays)

        self.t_m = 25
        self.t_n = 250
        self.t_a =  np.where(7 * self.stddev > self.t_m, 7 * self.stddev, self.t_m)

    def get_pixel_stddev(self, set):
        return np.sqrt(np.sum(np.power(set * 1. - set.mean(axis=0), 2), axis=0) / set.shape[0])

    def get_pixel_mean(self, set):
        return set.mean(axis=0)

    def decide(self, given, given_mask=None):
        gray = cv2.cvtColor(given, cv2.COLOR_BGR2GRAY).astype(np.float32).reshape(self.height, self.width, 1)
        mask = np.where((np.min(given_mask, axis=2) == -1).reshape(self.height, self.width, 1), BgI,
               np.where(np.abs(gray - self.mean) > self.t_a, FgI, BgI))
        return np.array(mask)

class BackgroundModel(object):
    def __init__(self, frames, n_frames):
        self.height, self.width, self.channels = frames[0].shape
        self.bcm = BCM(frames, n_frames)
        self.bim = BIM(frames, n_frames)
        self.bem = BEM(frames, n_frames)

    def extract_region(self, given):
        given = given.astype(np.uint8)
        _, cnts, _ = cv2.findContours(given, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        ret = np.zeros(given.shape)
        cv2.drawContours(ret, cnts, -1, 255, -1)
        return ret.astype(np.uint8)

    def decide(self, given):
        kernel = np.ones((5,5),np.uint8)

        gray = cv2.cvtColor(given, cv2.COLOR_BGR2GRAY).reshape(self.height, self.width, 1)
        inside = np.tile(np.where((gray < self.bim.t_n) & (gray > self.bim.t_m), 1, -1), (1,1,3))
        outside = np.tile(np.where((gray > self.bim.t_n) | (gray < self.bim.t_m), 1, -1), (1,1,3))

        inside_edge_mask = self.bem.decide(given, inside)
        inside_color_mask = self.bcm.decide_normal(given, inside)
        intensity_mask = self.bim.decide(given, outside)

        outside_brightness_mask = self.bcm.decide_extrem(given, outside)

        FgEdges_F = np.where(inside_edge_mask == FgE, 255, 0)
        FgEdges =  cv2.morphologyEx(FgEdges_F.reshape(FgEdges_F.shape[0], FgEdges_F.shape[1], 1).astype(np.uint8), cv2.MORPH_CLOSE, kernel, iterations=3)

        FgColor_C = np.where(inside_color_mask == FgC, 255, 0)
        FgColor_DF = np.where(outside_brightness_mask == DF, 255, 0)
        FgColor_LF = np.where(outside_brightness_mask == LF, 255, 0)
        FgColor_S = np.where(inside_color_mask == S, 255, 0)
        FgColor_H = np.where(inside_color_mask == H, 255, 0)

        KSH = FgColor_S | FgColor_H

        FgInt = np.where(intensity_mask == FgI, 255, 0)

        FgColor_Int =  cv2.morphologyEx((FgColor_C | FgColor_DF | FgColor_LF | FgInt).astype(np.uint8), cv2.MORPH_CLOSE, kernel).reshape(self.height, self.width, 1)

        Region_Final = self.extract_region(FgEdges).reshape(self.height, self.width, 1) | self.extract_region(FgColor_Int)

        FgEdges_Final = Region_Final & FgEdges.reshape(self.height, self.width, 1)

        FgChrom_Camuf = Region_Final & KSH

        return cv2.morphologyEx(np.tile(Region_Final | FgEdges_Final | FgChrom_Camuf, (1,1,3)).astype(np.uint8), cv2.MORPH_OPEN, kernel, iterations=2)
