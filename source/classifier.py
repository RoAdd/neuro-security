from __future__ import absolute_import
from os import getcwd, listdir, makedirs, unlink
from os.path import dirname, exists, isfile, join
from time import gmtime, strftime
from helper.helpers import pyramid
from helper.helpers import sliding_window
import numpy as np
import theano
from keras import models
from keras.callbacks import EarlyStopping
from keras.datasets import mnist
from keras.layers import containers
from keras.layers.convolutional import (Convolution2D, MaxPooling2D,
                                        UpSampling2D)
from keras.layers.core import (Activation, AutoEncoder, Dense, Dropout,
                               Flatten, Reshape)
from keras.models import Sequential
from PIL import Image
from PIL.ImageDraw import Draw
import pybing.build.bing
import dataset
from cae_1 import build_model
from helper.decode_elements import DePool2D
from helper.decode_elements import Deconvolution2D
from helper.decode_elements import DependentDense
from helper.helper import show_representations
import config

theano.config.optimizer = 'fast_compile'


def ensure_dir(f):
    d = dirname(f)
    if not exists(d):
        makedirs(d)


def clear_folder(f):
    for the_file in listdir(f):
        file_path = join(f, the_file)
        try:
            if isfile(file_path):
                unlink(file_path)
        except Exception, e:
            print e

if __name__ == '__main__':
    encoder_layers, decoder_layers, classification_layers = build_model()
    model = Sequential()
    for encoder in encoder_layers:
        model.add(encoder)
    for layer in classification_layers:
        model.add(layer)

    model.load_weights(SAVE_PATH)
    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop', class_mode='binary')

    images = []

    ensure_dir(CLASSIFICATION_PATH)
    ensure_dir(join(CLASSIFICATION_PATH, "unclassified"))
    ensure_dir(join(CLASSIFICATION_PATH, "classified"))
    files = listdir(join(CLASSIFICATION_PATH, "unclassified"))
    for f in files:
        images.append(Image.open(
            join(CLASSIFICATION_PATH, "unclassified", f), "r"))

    binger = bing.BING('build/ObjectnessTrainedModel', 2, 8, 2)

    for i, 	img in enumerate(images):
        aux = img.convert("L")
        boxes = binger.objectness(img)
        draw = Draw(img)
        for b in bbox[:200]:
            x1, y1, x2, y2 = [int(a) for a in b[:4]]
            data = aux.crop((x1, y1, x2, y2)).resize((48, 96), Image.BILINEAR)
            data = np.array(data.getdata())
            data = data.astype(np.float32)
            data = np.multiply(data, 1.0 / 255.0)
            data = data.reshape([-1, 1, 96, 48])
            if model.predict(data)[0] > THRESHOLD:
                draw.rectangle([x1, y1, x2, y2], outline=(256, 0, 0))

        img.save(join(CLASSIFICATION_PATH, "classified", strftime(
            "%a, %d %b %Y %H:%M:%S", gmtime()) + "-" + str(i) + ".png"))

    # for img in images:
    #     aux = img.convert("L")
    #     for _ in range(5):
    #         i = 0
    #         while(i <= img.width - int(math.floor(window_size[0]))):
    #             print i
    #             j = 0
    #             while(j <= img.height - int(math.floor(window_size[1]))):
    #                 print j
    #                 data = aux.crop((i, j, int(math.floor(window_size[0])), int(math.floor(window_size[1])))
    #                                 ).resize((48, 96), Image.BILINEAR)
    #                 data = np.array(data.getdata())
    #                 data = data.astype(np.float32)
    #                 data = np.multiply(data, 1.0 / 255.0)
    #                 data = data.reshape([-1, 1, 96, 48])
    #                 if model.predict(data)[0] > THRESHOLD:
    #                     boxes.append(
    #                         [i, j, i + window_size[0],  j + window_size[1]])
    #                 j += int(math.floor(strides[1]))
    #             i += int(math.floor(strides[0]))
    #         strides = tuple([multip * x for x in strides])
    #         window_size = tuple([multip * x for x in window_size])
    #
    #     draw = Draw(img)
    #     for box in boxes:
    #         draw.rectangle(box, outline=(256, 0, 0))
    #     img.save(join(CLASSIFICATION_PATH, "classified", strftime(
    #         "%a, %d %b %Y %H:%M:%S", gmtime()) + "-" + str(i) + ".png"))
