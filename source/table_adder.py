# -- coding: utf-8 --
import config
from datetime import datetime
import json
import urllib
import urllib2
import webbrowser

months = ["january","february","march","april","may","june","july","august","spetember","october","november","december"]
ip = "192.168.0.2"

def add_monthly_table():
    my_time_arr =  datetime.now().strftime('%Y-%m-%d-%H-%M').split("-",5);
    my_db = my_time_arr[0]+"_"+months[int(my_time_arr[1])-1];

    query_args = {'db' : my_db}


    encoded_args = urllib.urlencode(query_args)
    url = "http://"+config.IP+"/index.php/ajaxController/addTable/monthly"
    print urllib2.urlopen(url, encoded_args).read()


def add_yearly_table():
    my_time_arr =  datetime.now().strftime('%Y-%m-%d-%H-%M').split("-",5);
    my_db = my_time_arr[0];

    query_args = {'db' : my_db}

    encoded_args = urllib.urlencode(query_args)
    url = "http://"+config.IP"+/index.php/ajaxController/addTable/yearly"
    print urllib2.urlopen(url, encoded_args).read()


add_monthly_table()
