import config
import os
from os import getcwd
from os.path import join
import numpy as np
import random
from keras.models import Sequential

import dataset

import yolo_wrapper

from cae_1 import build_model

from Tkinter import Button, Frame, Label, Tk

from PIL import Image, ImageTk, ImageDraw
from tkFileDialog import *
# from multibox.multibox import multibox
import cv2

import background_substraction_tools
import time_insert
# import caffe
#
# from fast_rcnn.config import cfg
# from fast_rcnn.test import im_detect
# from fast_rcnn.nms_wrapper import nms


label = None
img = None


encoder_layers, decoder_layers, classification_layers = build_model()
model = Sequential()
for encoder in encoder_layers:
    model.add(encoder)
for layer in classification_layers:
    model.add(layer)
model.load_weights(config.MODEL_SAVE_PATH)
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop', class_mode='binary')
cap = cv2.VideoCapture(0)


# config.bing_init()
# bing = Bing(config.BING_1ST_WEIGHTS,
#             config.BING_SIZES_INDECES_FN,
#             config.BING_2ND_WEIGHTS,
#             num_bbs_per_size_1st_stage = config.BING_1ST_BBS_COUNT,
#             num_bbs_final = config.BING_2ND_BBS_COUNT)

# prototxt = config.FASTER_RCNN_PROTO_PATH
# caffemodel = config.FASTER_RCNN_MODEL_PATH
# caffe.set_mode_gpu()
# caffe.set_device(0)
# cfg.GPU_ID = 0
# cfg.TEST.HAS_RPN = True
# net = caffe.Net(prototxt, caffemodel, caffe.TEST)

cap.read()
# def draw_box(loc, height, width, draw):
#     """A utility function to help visualizing boxes."""
#     xmin = int(round(loc[0] * width))
#     ymin = int(round(loc[1] * height))
#     xmax = int(round(loc[2] * width))
#     ymax = int(round(loc[3] * height))
#     img_box = img.crop((xmin, ymin, xmax, ymax)).resize(
#         (dataset.WIDTH, dataset.HEIGHT), Image.BILINEAR).convert("L")
#     data = np.array(img_box.getdata())
#     data = data.astype(np.float32)
#     data = np.multiply(data, 1.0 / 255.0)
#     data = data.reshape([-1, 1, 96, 48])
#     print model.predict(data)[0][0]
#     if(model.predict(data)[0][0] > config.CLASSIFICATION_THRESHOLD):
#         print 1
#         draw.rectangle([xmin, ymin, xmax, ymax], outline=(0, 255, 0))
th = 30




def show_results(img, results, img_width, img_height):
    img_cp = Image.fromarray(img)
    for i in range(len(results)):
        x = int(results[i][1])
        y = int(results[i][2])
        w = int(results[i][3])//2
        h = int(results[i][4])//2
        xmin = x-w
        xmax = x+w
        ymin = y-h
        ymax = y+h
        if xmin<0:
        	xmin = 0
        if ymin<0:
        	ymin = 0
        if xmax>img_width:
        	xmax = img_width
        if ymax>img_height:
        	ymax = img_height
        if((xmax - xmin) * 1.0 / (ymax - ymin + 1) > 1 or (xmax - xmin) * 1.0 / (ymax - ymin + 1) < 0.25): #or results[i][0] != "person":
            continue
        img_box = img_cp.crop((xmin, ymin, xmax, ymax)).resize(
            (dataset.WIDTH, dataset.HEIGHT), Image.BILINEAR).convert("L")
        data = np.array(img_box.getdata())
        data = data.astype(np.float32)
        data = np.multiply(data, 1.0 / 255.0)
        data = data.reshape([-1, 1, 96, 48])
        v = model.predict(data)[0][0]
        if(v > config.CLASSIFICATION_THRESHOLD):
            cv2.rectangle(img,(xmin, ymin),(xmax,ymax),color=(0, 0, 255),thickness=3)
            cv2.rectangle(img,(xmin,ymin-20),(xmax,ymin),(125,125,125),-1)
            cv2.putText(img,"Person:" + ' : %.2f' % v,(xmin+5,ymin-7),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,0),1)
    return img

def up(event):
    global th
    th += 1
    th = th % 256

def down(event):
    global th
    th -= 1
    th = th % 256

def update(event = None):
    n_frames = 60
    frames = None
    height, width, channels =  cap.read()[1].shape
    for i in range(n_frames):
        if frames is None:
            frames = cap.read()[1].reshape(1, height, width, channels)
        else:
            frames = np.append(frames, cap.read()[1].reshape(1, height, width, channels), axis=0)

    mod = background_substraction_tools.BackgroundModel(frames, n_frames)

    while(True):
        ret, frame = cap.read()
        # frame = cv2.flip(frame, 1)
        # frame  = cv2.medianBlur(frame,3)

        # results = yolo_wrapper.get_boxes(frame)

        final = mod.decide(frame)

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # proc_img = show_results(frame, results, frame.shape[1], frame.shape[0])
        # print proc_img.shape
        _, cnts, _ = cv2.findContours(np.min(final.astype(np.uint8), axis=2), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)


        orig_img = Image.fromarray(frame)
        orig_tk = ImageTk.PhotoImage(image=orig_img)
        contor = 0
        for c in cnts:
            x,y,w,h = cv2.boundingRect(c)
            xmin = x
            ymin = y
            xmax = x + w
            ymax = y + h

            img_box = orig_img.crop((xmin, ymin, xmax, ymax)).resize(
                (dataset.WIDTH, dataset.HEIGHT), Image.BILINEAR).convert("L")
            data = np.array(img_box.getdata())
            data = data.astype(np.float32)
            data = np.multiply(data, 1.0 / 255.0)
            data = data.reshape([-1, 1, 96, 48])
            v = model.predict(data)[0][0]
            if(v > config.CLASSIFICATION_THRESHOLD):
                contor += 1
                cv2.rectangle(frame,(xmin, ymin),(xmax,ymax),color=(0, 0, 255),thickness=3)
                cv2.rectangle(frame,(xmin,ymin-20),(xmax,ymin),(125,125,125),-1)
                cv2.putText(frame,"Person:" + ' : %.2f' % v,(xmin+5,ymin-7),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,0),1)


        # time_insert.getTime("Hol", contor)

        final_img = Image.fromarray(frame)
        final_tk = ImageTk.PhotoImage(image=final_img)

        mask_img = Image.fromarray(final)
        mask_tk = ImageTk.PhotoImage(image=mask_img)

        orig_label.configure(image=orig_tk)
        orig_label.image = orig_tk
        final_label.configure(image=final_tk)
        final_label.image = final_tk
        mask_label.configure(image=mask_tk)
        mask_label.image = mask_tk

        root.update()
    cap.release()


dataset.init_consts()
root = Tk()

mf = Frame(root, width=600, height=600)
mf.bind("<Up>",up)
mf.bind("<Down>",down)
mf.focus_set()
mf.pack()
orig_label = Label(mf, image=img)
orig_label.configure(image=img)
orig_label.image = img
orig_label.grid(row=0, column=0)
final_label = Label(mf, image=img)
final_label.configure(image=img)
final_label.image = img
final_label.grid(row=0, column=1)
mask_label = Label(mf, image=img)
mask_label.configure(image=img)
mask_label.image = img
mask_label.grid(row=1, column=0)
root.after(100, update)
root.mainloop()
